import React, { useEffect, useState } from 'react';
import './App.css';
import { BrowserRouter, Routes, Route, Switch } from 'react-router-dom';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import UserPage from './ userPage';
import AdminPage from './pages/mainpage';

const App = () => {
  //console.log(process.env.REACT_APP_API_HOST, process.env.NODE_ENV)
  const notify = () =>
    toast.success('Done !', {
      position: 'top-right',
      className: 'toastlify',
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });

  const nghiengngoc = 'Ngoc ngoc';

  useEffect(() => {}, []);
  return (
    <div className="App">
      {/*<BrowserRouter basename="/admin">
                <AdminPage notify={notifySuccess}/>
            </BrowserRouter>*/}
      <BrowserRouter basename="/">
        <UserPage notify={notify} />
      </BrowserRouter>
    </div>
  );
};

export default App;
