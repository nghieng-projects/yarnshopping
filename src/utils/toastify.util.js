import {toast} from "react-toastify";

export const notifySuccess = (message) => toast.success(message, {
    position: 'top-right',
    className: 'toastlify',
    autoClose: 1000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
});