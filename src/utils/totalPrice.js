

export const calculatePrice = (userCart) => {
    const arrPrice = []
    if(userCart.length>0){
        userCart.map((item) => {
            arrPrice.push(item.price * item.quantity)
        })
        const finalPrice = arrPrice.reduce((prev, next) => {
            return prev + next
        })
        return finalPrice
    }
    return 0
}