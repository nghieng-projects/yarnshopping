const apiHost = process.env.REACT_APP_API_HOST;
export const getApi = () => {
  return fetch(`${apiHost}/database`).then((response) => {
    return response.json();
  });
};

export const getApiBaseId = (id) => {
  return fetch(`${apiHost}/database/${id}`).then((response) => {
    return response.json();
  });
};

export const updateApi = (id, product) => {
  return fetch(`${apiHost}/database/${id}`, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(product),
  }).then((response) => {
    return response.json();
  });
};

export const createApi = (product) => {
  return fetch(`${apiHost}/database`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(product),
  }).then((response) => {
    return response.json();
  });
};
export const deleteApi = (id) => {
  return fetch(`${apiHost}/database/${id}`, {
    method: 'DELETE',
  }).then((response) => {
    return response.json();
  });
};
export const filterApi = (string) => {
  return fetch(`${apiHost}/database/?q=${string}`).then((response) => {
    return response.json();
  });
};
