const apiHost = process.env.REACT_APP_API_HOST;
export const postUserPayment = (payment) => {
    return fetch(`${apiHost}/payment`,{
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(payment)
    }).then((response)=>{
        return response.json()
    })
}
export const updateUserPayment = (id,payment) => {
    return fetch(`${apiHost}/payment/${id}`,{
        method:'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(payment)
    }).then((response) => {
        return response.json()
    })
}

export const getPaymentBaseId = (id) => {
    return fetch(`${apiHost}/payment/${id}`)
        .then((response)=>{
            return response.json()
        })
}