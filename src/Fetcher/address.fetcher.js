const apiHost = process.env.REACT_APP_API_HOST
export const getProvinces = () => {
    return fetch(`${apiHost}/provinces`)
        .then((response)=>{
            return response.json()
        })
}
export const getWards = (code) => {
    return fetch(`${apiHost}/wards?parent_code=${code}`)
        .then((response)=>{
            return response.json()
        })
}
export const getDistricts = (code) => {
    return fetch(`${apiHost}/districts?parent_code=${code}`)
        .then((response)=>{
            return response.json()
        })
}
