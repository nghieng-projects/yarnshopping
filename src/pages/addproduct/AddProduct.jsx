import React, {useEffect, useState} from 'react';
import './AddProduct.scss';
import Input from "../../components/Input";
import TextArea from "../../components/TextArea";
import Select from "../../components/Select";
import Button from "../../components/Button";
import Checkbox from "../../components/Checkbox";
import {getApiBaseId, createApi,updateApi, getApi} from "../../Fetcher/yarnshopping.fetcher";
import {NavLink, useLocation} from "react-router-dom";
import {ToastContainer} from "react-toastify";

const category = ["Yarns", "Tools", "Ready products","New","Sale"];
const subCategory = ["Cotton","Milk","Jean", "Other"]
const unit = ["USD", "EUR", "VND"]
const status = ["Active", "Archived", "Not available"]

const useQuery = () => {
    const {search} = useLocation();
    return React.useMemo(() => new URLSearchParams(search), [search]);
};

const AddProduct = ({notify}) => {
    const query = useQuery();
    const id = query.get('id');


    const [products, setProducts] = useState({
        product: "",
        description: "",
        file: "",
        tags: "",
        price: 0,
        quantity: 0,
        export: 0,
        isPublic: false,
        category: category[0],
        subCategory: subCategory[0],
        unit: unit[0],
        status: status[0]
    })
    const date = new Date();
    const getDay = date.getDay() + 1
    const getMonth = date.getMonth() + 1
    const getYear = date.getFullYear()
    const time = `${getDay}.${getMonth}.${getYear}`

    const handleOnchange = ({target}) => {
        const value = target.value
        const name = target.name
        setProducts((prevState) => {
            return {...prevState, [name]: value, createAt: time , finalQuantity: prevState.quantity - prevState.export}
        })
    }

    const onCheckboxChange = ({target}) => {
        const value = products.isPublic
        const name = target.name;
        setProducts((prevState) => {
            return {...prevState, [name]: !value}
        })
    }


    const createHandle = () => {
        if(id){
            updateApi(id,products).then((data)=>{
                getApi()
                notify()
            })
        }else {
            if (products.product && products.description && products.tags && products.price && products.quantity && products.category && products.subCategory && products.unit && products.status) {
                createApi(products).then((data) => {
                    console.log(data)
                    getApi()
                    notify()
                })
            }else{
                alert ('Not enough information, check again !')
            }
        }
    }
    const getData = () => {
        if (id) {
            console.log({id})
            getApiBaseId(id).then((data) => {
                console.log({data});
                setProducts(data)
            })
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <div className="AddProduct">
            <div className="boxAddProduct">
                <div>
                    <p>Product Title</p>
                    <Input name="product" type="text" value={products.product} placeholder="Type here"
                           handleChange={(event) => {
                               handleOnchange(event)
                           }}/>
                </div>
                <div>
                    <p>Full description</p>
                    <TextArea placeholder="Type here" name="description" value={products.description}
                              handleChange={(event) => {
                                  handleOnchange(event)
                              }}/>
                </div>
                <div>
                    <p>Quantity</p>
                    <Input name="quantity" type="number" value={products.quantity} placeholder="Enter number"
                           handleChange={(event) => {
                               handleOnchange(event)
                           }}/>
                </div>
                <div>
                    <p>Images</p>
                    <div>
                        {/*<InputFile type="file" name="file" value={products.file} handleChange={(event)=>{handleOnchange(event)}}/>*/}
                    </div>
                </div>
                <div>
                    <p>Tags</p>
                    <Input type="text" name="tags" value={products.tags} handleChange={(event) => {
                        handleOnchange(event)
                    }}/>
                </div>
                <div className="flexItem justify1">
                    <div>
                        <p>Category</p>
                        <Select options={category} selected={products.category} name="category"
                                style="addProductSelectSize" handleSelect={(event) => {
                            handleOnchange(event)
                        }}/>
                    </div>
                    <div>
                        <p>Sub-category</p>
                        <Select options={subCategory} selected={products.subCategory} name="subCategory"
                                style="addProductSelectSize" handleSelect={(event) => {
                            handleOnchange(event)
                        }}/>
                    </div>
                </div>
                <div>
                    <p>Price</p>
                    <div className="flexItem justify1">
                        <div>
                            <Input type="text" name="price" value={products.price} handleChange={(event) => {
                                handleOnchange(event)
                            }}/>
                        </div>
                        <div>
                            <Select options={unit} selected={products.unit} name="unit" handleSelect={(event) => {
                                handleOnchange(event)
                            }}/>
                        </div>
                        <div>
                            <Select options={status} selected={products.status} name="status" handleSelect={(event) => {
                                handleOnchange(event)
                            }}/>
                        </div>
                    </div>

                </div>
                <div>
                    <Checkbox value={products.isPublic} checked={products.isPublic} name="isPublic"
                              handleChange={(event) => {
                                  onCheckboxChange(event)
                              }}/> Publish on website
                </div>
                <div className='group'>
                    <div>
                        <a><Button onClick={createHandle} text={'Submit item'} style={'primary'}/></a>
                    </div>
                   <div>
                       <NavLink to="/product"><Button text={'Close'} style={'primary'}/></NavLink>
                       <ToastContainer
                           position="top-right"
                           autoClose={5000}
                           hideProgressBar={false}
                           newestOnTop={false}
                           closeOnClick
                           rtl={false}
                           pauseOnFocusLoss
                           draggable
                           pauseOnHover
                       />
                   </div>
                </div>
            </div>
        </div>
    )
}
export default AddProduct