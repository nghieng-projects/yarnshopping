import React, {useState} from 'react'
import './AdminPages.scss'
import {
    BrowserRouter,
    Routes,
    Route,
} from "react-router-dom";
import AddProduct from "../addproduct";
import Product from "../product/Product";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



const AdminPage = () => {
    const notify = () => toast.success("Successfull !", {
        position: 'top-right',
        className:'toastlify',
        autoClose: 1000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    });

    return (
        <div className="Main">
            <Routes>
                {/*<Route path='/add-product' element={<AddProduct notify={notify}/>}/>
                <Route path='/product' element={<Product />}/>*/}
            </Routes>

        </div>
    )
}
export default AdminPage