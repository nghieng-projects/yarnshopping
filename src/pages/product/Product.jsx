import React from 'react';
import './Product.scss';
import Table from "../../components/Table";
import Button from "../../components/Button";
import {useNavigate} from "react-router-dom";

const Product = () => {
    const navigate = useNavigate()
    const handleClickCreate = () => {
        navigate('/add-product')
    }

    return(
        <div className="Product">
            <div className="createButton">
                <Button text="+ Create new" style="primary" onClick={handleClickCreate} />
            </div>
            <Table />
        </div>
    )
}
export default Product