import React, {useState} from 'react'
import Button from "../../../../components/Button";
import './ActionButton.scss'

const ActionButton = ({handleToggleClick,show}) => {


    return (
        <div className="actionButton">
                <Button text={'...'} onClick={handleToggleClick} style="secondary"/>
            {
                show && (<ul className="child">
                    <li>View detail</li>
                    <li>Edit info</li>
                    <li>Delete</li>
                </ul>)
            }
        </div>
    )
};
export default ActionButton