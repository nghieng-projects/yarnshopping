import React from "react";
import './Checkbox.scss';
const Checkbox = ({name,value,handleChange,...rest}) => {

    return (
        <input
            type="checkbox"
            name={name}
            value={value}
            onChange={(value)=>{handleChange(value)}}
            {...rest}/>
    )
}
export default Checkbox