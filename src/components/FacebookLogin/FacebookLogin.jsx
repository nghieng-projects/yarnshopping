import React from 'react';
import './FacebookLogin.scss'
import PropTypes from "prop-types";
import Button from "../Button";

const FacebookLogin = ({text,className}) => {
    return(
        <Button text={<div className="signupButton">
            <div className="icon iconFacebook" />
            <div className="text">{text}</div>
        </div>}  style="secondary" className={className}/>

    )
}
FacebookLogin.propTypes = {

}
export default FacebookLogin
