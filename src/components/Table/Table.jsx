import React, {useState} from 'react';
import './Table.scss'
import Select from "../Select";
import Input from "../Input";
import TableData from "../TableData";
import {filterApi, getApi} from "../../Fetcher/yarnshopping.fetcher";

const itemOptions = ["All category", "Yarns", "Tools", "Ready Products","Sale","New"]
const statusOptions = ["Status", "Active", "Archived","Not available", "Show all"]

const Table = () => {
    const [filterData,setFilterData] = useState([])
    const handleChange = (event) => {
        const value = event.target.value;
        console.log({value})
        filterApi(value).then((data)=>{
            console.log({data})
            setFilterData(data)
        })
    }
    const selectOnChange = ({target}) => {
        const value = target.value;
        console.log({value})
        switch (value){
            case "All category":
            case "Show all":
            case "Status":
                getApi().then((data)=>{
                    setFilterData(data)
                })
                break
            case "Yarns":
            case "Tools":
            case "Ready Products":
            case "Sale":
            case "New":
            case "Active":
            case "Archived":
            case "Not available":
                filterApi(value).then((data)=>{
                    setFilterData(data)
                })
                break
        }
    }


    return (
        <div className="productPage">
            <div className="filterProduct">
                <div className="header">
                    <Input placeholder={`Search on items`} handleChange={handleChange} type="text"/>
                </div>
                <div className="header">
                    <Select options={itemOptions} handleSelect={(event)=>{selectOnChange(event)}}/>
                    <Select options={statusOptions} handleSelect={(event)=>{selectOnChange(event)}}/>
                </div>
            </div>
            <div className="table">
                <TableData filterData={filterData}/>
            </div>
        </div>
    )
}
export default Table