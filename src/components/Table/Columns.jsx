import React, {useState} from 'react';
import Button from "../Button";
import ActionButton from "../../pages/product/components/ActionButton";
import StatusButton from "../StatusButton";

import OutsideClickHandler from 'react-outside-click-handler';

const columns = [
    {
        Header: "...",
        accessor: "checkbox",
        Cell: () => {
            return (
                <input type="checkbox"/>
            )
        }
    },
    {
        Header: "ID",
        accessor: "id"
    },
    {
        Header: "Product",
        accessor: "product"
    },
    {
        Header: "Category",
        accessor: "category"
    },
    {
        Header: "Status",
        accessor: "status",
        Cell: (item) => {
            return (
                <StatusButton text={item.value}/>
            )
        }
    },
    {
        Header: "Date",
        accessor: "date"
    },
    /*{
        Header: "Detail",
        accessor: "detail",
        Cell: () => {
            return (
                <div className="productButton">
                    <Button text={'Detail'}/>
                </div>

            )
        }
    },*/
    {
        Header: "Action",
        accessor: "action",
        Cell: () => {
            const [isShow, setIsShow] = useState(false)
            const toggleClick = () => {
                setIsShow(!isShow)
            }

            return (
                <div className="productButton">
                    <div className="detailButton">
                        <Button text={'Detail'} style="secondary"/>
                    </div>
                    <div className="boxActionBtn">
                        <OutsideClickHandler onOutsideClick={()=>{setIsShow(false)}}>
                            <ActionButton handleToggleClick={toggleClick} show={isShow}/>
                        </OutsideClickHandler>
                    </div>

                </div>

            )
        }
    }
]
export default columns