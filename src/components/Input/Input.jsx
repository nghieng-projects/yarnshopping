import React from 'react';
import './Input.scss';

const Input = ({
  type,
  name,
  value,
  placeholder,
  handleChange,
  isRequired,
  ...rest
}) => {
  return (
    <input
      className="inputSpace"
      type={type}
      required={isRequired}
      name={name}
      value={value}
      placeholder={placeholder}
      onChange={(value) => {
        handleChange(value);
      }}
      {...rest}
    />
  );
};
export default Input;
