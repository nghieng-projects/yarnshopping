import React from 'react';
import './SampleProduct.scss';
import {NavLink} from "react-router-dom";
import pic1 from '../../assets/images/pic1.jpg'

const SampleProduct = ({products,text}) => {
    const typeProduct = products

    return (
        <div className='SampleProduct'>
            <div className='boxProduct'>
                <div className='category'>
                    <p>{text}</p>
                </div>
                <div className='displayProduct'>
                    {
                         typeProduct.map((product,index) => {
                            return (
                                <div className='product'>
                                    <NavLink to={`/detail/?id=${product.id}`} key={index}>
                                        <div className='itemProduct'>
                                            <div className='img'>
                                                <img src={pic1} alt=""/>
                                            </div>
                                            <div className='information'>
                                                <p>{product.product}</p>
                                                <p>${product.price}</p>
                                                <p>{product.category}</p>
                                                {/*<p>id:{product.id}</p>*/}
                                            </div>
                                        </div>
                                    </NavLink>
                                </div>
                            )
                        })
                    }
                </div>
            </div>

        </div>
    )
}
export default SampleProduct
