import React from 'react';
import './TextArea.scss';
const TextArea = ({placeholder,name,value,handleChange}) => {
    return(
        <textarea className="TextArea" name={name} value={value} placeholder={placeholder} rows="5" onChange={(value)=>{handleChange(value)}}/>
    )
}
export default TextArea