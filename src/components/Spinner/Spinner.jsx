import React from "react";
import './Spinner.scss';
import spinner from '../../assets/icons/spinner.gif';
const Spinner = () => {
    return(
        <div className='spinner'>
            <img src={spinner} alt=""/>
        </div>
    )
}
export default Spinner