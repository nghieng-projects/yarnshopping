import React from "react";
import cln from 'classnames';
import PropTypes from 'prop-types';
import './Button.scss'

const Button = ({text,disable, style, onClick,className}) => {
    return(
        <button className={cln('button', style, className)} disabled={disable} onClick={onClick}>{text}</button>
    )
}
Button.propTypes = {
    text: PropTypes.node.isRequired,
    style: PropTypes.string,
    onClick: PropTypes.func.isRequired
}
Button.defaultProps = {
    style:"default"
}
export default Button



