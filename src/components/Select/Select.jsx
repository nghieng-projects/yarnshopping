import React from 'react';
import cln from "classnames";
import './Select.scss';
const Select = ({name,options,style,handleSelect,selected,placeholder}) => {
    return (
        <select className={cln("SelectButton",style)} name={name} id="" onChange={(unit)=>{handleSelect(unit)}} >
            <option>{placeholder}</option>
            {
                options.map((option) => {
                    return (
                        <option selected={selected === option ? true : false} value={option} key={option}>{option}</option>
                    )
                })
            }

        </select>
    )
}
export default Select