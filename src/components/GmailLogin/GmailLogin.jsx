import React from 'react';
import './GmailLogin.scss';
import Button from "../Button";
 const GmailLogin = ({text, className}) => {
    return (
    <Button text={<div className="signupButton">
        <div className="icon iconGmail" />
        <div className="text">{text}</div>
    </div>} style="secondary" className={className}/>
    )
}
GmailLogin.propTypes = {

}
export default GmailLogin