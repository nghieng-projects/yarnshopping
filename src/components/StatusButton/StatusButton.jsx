import React, {useState} from 'react';
import './StatusButton.scss'
import cln from 'classnames'
import button from "../Button";
const StatusButton = ({text}) => {
    let style = null
    if(text === "Active"){
        style = "activeButton"
    }else if(text === "Archived"){
        style = "archivedButton"
    }else{
        style = "notAvailableButton"
    }

    return(
        <div className="defaultButton">
            <button className={cln("default",style)}>{text}</button>
        </div>

    )
}
export default StatusButton