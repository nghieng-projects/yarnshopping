import React from "react";
import './DetailActionBtn.scss'

const DetailActionBtn = ({handleClick}) => {

    return(
        <ul className="detailBtn">
            <li onClick={()=>{handleClick('edit')}}>Edit</li>
            <li onClick={()=>{handleClick('delete')}}>Delete</li>
        </ul>
    )
}
export default DetailActionBtn