import React, {useEffect, useState} from "react";
import PostData from "./PostData";
import Pagination from "./Pagination";
import {getApi, deleteApi} from "../../Fetcher/yarnshopping.fetcher";


const TableData = ({filterData}) => {
    console.log({filterData})

    const [posts, setPosts] = useState([])
    const [loading, setLoading] = useState(false)
    const [currentPage, setCurrentPage] = useState(1)
    const [postPerPage] = useState(5)
    const [totalData, setTotalData] = useState([])
    const indexLastPost = currentPage * postPerPage
    const indexFirstPost = indexLastPost - postPerPage
    const getTableData = () => {
        getApi().then((data) => {
            console.log({data})
            setTotalData(data)
            const currentPost = data.slice(indexFirstPost, indexLastPost)
            setPosts(currentPost)
            console.log({currentPage, indexFirstPost, indexLastPost, currentPost});
        })
    }
    const deleteItem = (id) => {
        deleteApi(id).then((data) => {
            console.log({data})
            getTableData()
        })
    }

    useEffect(() => {
        setLoading(true)
        getTableData()
        setLoading(false)
    }, [currentPage])

    useEffect(()=>{
        setPosts(filterData)
        setTotalData(filterData)
    },[filterData])

    //Change page
    const pagination = (pageNumber) => {
        setCurrentPage(pageNumber)

    }

    return (
        <div>
            <PostData post={posts} loading={loading} handleDelete={deleteItem}/>
            <div>
                <Pagination postPerPage={postPerPage} totalPost={totalData.length} pagination={pagination}/>
            </div>
        </div>
    )
}
export default TableData