import React, {useState} from "react";
import Checkbox from "../Checkbox";
import StatusButton from "../StatusButton";
import Button from "../Button";
import DetailActionBtn from "./DetailActionBtn";
import {useNavigate} from "react-router-dom";

const TableRow = ({item, id, handleDelete}) => {
    const navigate = useNavigate()
    const [isShow, setShow] = useState(false)
    const toggleShowButton = () => {
        setShow(!isShow)
    }
    const handleClick = (action) => {
        console.log({action})
        setShow(false)
        switch (action) {
            case 'edit':
                navigate(`/add-product/?id=${id}`)
                break;
            case 'delete':
                console.log({id})
                const confirm = window.confirm('do u want to delete this item ?')
                if (confirm) {
                    handleDelete(id)
                }
                break;
        }
    }

    return (
        <tr className="itemRow">
            <td className='body'><Checkbox/></td>
            <td className='body'>{item.id}</td>
            <td>{item.product}</td>
            <td>{item.category}</td>
            <td>{item.subCategory}</td>
            <td>{item.price}</td>
            <td>{item.quantity}</td>
            <td>{item.export}</td>
            <td>{item.finalQuantity}</td>
            <td><StatusButton text={item.status}/></td>
            <td>{item.isPublic === true ? (<p>Public</p>) : (<p>Not public</p>)}</td>
            <td>{item.createAt}</td>
            <td>
                <div className='productButton'>
                    <Button text='Detail'/>
                    <Button text='...' onClick={toggleShowButton}/>
                    {isShow ? (<DetailActionBtn handleClick={handleClick}/>) : ""}
                </div>
            </td>
        </tr>
    )
}
export default TableRow