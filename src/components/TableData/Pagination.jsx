import React from "react";
import './Pagination.scss'

const Pagination = ({postPerPage, totalPost, pagination}) => {
    console.log({totalPost})
    const pageNumber = [];
    for (let i = 1; i <= Math.ceil(totalPost / postPerPage); i++) {
        pageNumber.push(i)
    }
    return (
        <ul className="numberPagination">
            {pageNumber.map((number) => {
                return (

                    <li key={number}>
                        <a href="#" onClick={() => {
                            pagination(number)
                        }}>{number}</a>
                    </li>
                )
            })}
        </ul>
    )
}
export default Pagination