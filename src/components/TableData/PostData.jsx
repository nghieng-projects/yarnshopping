import React from 'react';
import './PostData.scss'
import TableRow from "./TableRow";


const titleTable = ['id', 'product', 'category','subcategory','price','quantity','export','finalQuantity','status','public', 'date','action']

const PostData = ({post, loading,handleDelete}) => {
    console.log({post})
    if (loading) {
        return <h2>Loading...</h2>
    }

    return (
        <div className="table">
            <table cellPadding="0" cellSpacing="0">
                <thead>
                <tr>
                    <th className='title'>...</th>
                    {titleTable.map((title, index) => {
                        return (
                            <th className='title'>{title}</th>
                        )
                    })}
                </tr>
                </thead>
                <tbody>
                {post.map((item) => {
                    return (
                        <TableRow item={item} id={item.id} handleDelete={handleDelete}/>
                    )
                })}
                </tbody>
            </table>

        </div>
    )
}
export default PostData