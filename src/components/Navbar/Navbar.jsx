import React from "react";

import {NavLink} from "react-router-dom";


const Navbar = ({options,style}) => {
    return (
        <div className={style}>
            <ul >
                {
                    options.map((item, index) => {
                        return (
                            <NavLink to={`/item/${item.category}`} key={index}>
                                <li value={item}>{item.label}</li>
                            </NavLink>

                        )
                    })
                }
            </ul>
        </div>
    )
}
export default Navbar