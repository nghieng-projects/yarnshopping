import React, {useState} from 'react';
import './QuantityButton.scss'
const QuantityButton = ({value,style, onChangeValue}) => {
    const handlePlus = () => {
        onChangeValue(value + 1)
    }
    const handleMinus = () => {
        if (value > 0) {
            onChangeValue(value - 1)
        }
    }
    const handleChange = (event) => {
        const newValue = parseInt(event.target.value)
        onChangeValue(newValue)
    }

    return(
        <div className={style}>
            <div className='groupButton'>
                <button className="symbol minus" type="button" onClick={handleMinus}>-</button>
            </div>
            <input className="number border" type="number" value={value} onChange={handleChange} />
            <div className='groupButton'>
                <button className="symbol plus" type="button" onClick={handlePlus}>+</button>
            </div>
        </div>
    )
}
export default QuantityButton