import React from 'react';
import './TextFreeship.scss';
const TextFreeship = ({ bill }) => {
  const moneyLeft = 50 - bill;
  return (
    <div className="TextFreeship">
      {bill >= 50 ? (
        <div className="text">
          <p>Your invoice have freeship promotion</p>
        </div>
      ) : (
        <div className="text">
          <p>Your invoice need ${moneyLeft} dollars for freeship</p>
        </div>
      )}
    </div>
  );
};
export default TextFreeship;
