import React from 'react';
import './InputFile.scss';

const InputFile = ({type,name,value,handleChange}) => {
    return(
        <input type={type} name={name} value={value} onChange={(value)=>{handleChange(value)}} className="InputFile"/>
    )
}
export default InputFile