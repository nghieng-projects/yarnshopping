import React from "react";
import cln from 'classnames'
import './IndicatorItem.scss';

const IndicatorItem = ({item,isActive}) => {

    return (
        <div className={cln('indicatorItem', {'isActive': isActive})}>
            <div className='itemProgress'>
                <div className='circle'>
                    <span>{item.name}</span>
                </div>
                <div className='single-line'></div>
            </div>
        </div>

    )
}
export default IndicatorItem