import React from "react";
import './ProgressIndicator.scss';
import IndicatorItem from "./IndicatorItem";


const ProgressIndicator = ({steps, currentStep}) => {

    return (
        <div className='progressIndicator'>
            {
                steps.map((item, index)=>{
                    return(
                        <IndicatorItem key={index} item={item} isActive={index <= currentStep}/>
                    )
                })
            }

        </div>
    )
}
export default ProgressIndicator