import React, { useEffect, useState } from 'react';
import './HomePage.scss';
import { getApi } from '../../Fetcher/yarnshopping.fetcher';
import SampleProduct from '../../components/SampleProduct';
import Spinner from '../../components/Spinner';
import AdminPage from '../../pages/mainpage';
import { BrowserRouter } from 'react-router-dom';
import { notifySuccess } from '../../utils/toastify.util';

const HomePage = () => {
  //Modal
  const [yarns, setyarns] = useState([]);
  const [tools, setTools] = useState([]);
  const [readyProduct, setReadyProduct] = useState([]);
  const [stateNew, setNew] = useState([]);
  const [sale, setSale] = useState([]);
  const [loading, setLoading] = useState(false);
  const setStateProducts = () => {
    setLoading(true);
    getApi().then((data) => {
      setyarns(
        data.filter((item) => {
          return item.category === 'Yarns';
        })
      );
      setTools(
        data.filter((item) => {
          return item.category === 'Tools';
        })
      );
      setReadyProduct(
        data.filter((item) => {
          return item.category === 'Ready Products';
        })
      );
      setNew(
        data.filter((item) => {
          return item.category === 'New';
        })
      );
      setSale(
        data.filter((item) => {
          return item.category === 'Sale';
        })
      );
      //console.log(data)
      setLoading(false);
    });
  };

  useEffect(() => {
    setStateProducts();
  }, []);

  return (
    <div className="HomePage">
      <div className="body">
        {loading === false ? (
          <div className="itemProduct">
            <SampleProduct products={yarns} text={'Yarns'} />
            <SampleProduct products={tools} text={'Tools'} />
            <SampleProduct products={readyProduct} text={'ReadyProduct'} />
            <SampleProduct products={stateNew} tetx={'New'} />
            <SampleProduct products={sale} text={'Sale'} />
          </div>
        ) : (
          <Spinner />
        )}
      </div>
    </div>
  );
};
export default HomePage;
