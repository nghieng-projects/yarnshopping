import React, { useState } from 'react';
import './Header.scss';
import Navbar from '../../components/Navbar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faHouseChimney,
  faMagnifyingGlass,
  faShoppingBag,
  faUser,
  faTruck,
} from '@fortawesome/free-solid-svg-icons';
import { faAlignJustify } from '@fortawesome/free-solid-svg-icons';
import Modal from 'react-modal';
import ModalShoppingCart from '../modalShoppingCart';
import { NavLink } from 'react-router-dom';
import cln from 'classnames';
import OutsideClickHandler from 'react-outside-click-handler';

const navbar = [
  {
    label: 'Yarns',
    category: 'yarns',
  },
  {
    label: 'Tools',
    category: 'tools',
  },
  {
    label: 'Ready Products',
    category: 'ready product',
  },
  {
    label: 'New',
    category: 'new',
  },
  {
    label: 'Sale',
    category: 'sale',
  },
];

const Header = ({
  openModal,
  isOpenModal,
  isCloseModal,
  cart,
  setCart,
  totalPrice,
  setTotalPrice,
  setStateProducts,
  handleChange,
  handleChangeSearchInput,
  handleSearchIcon,
}) => {
  const [isShow, setShow] = useState(false);

  return (
    <div className="header-homepage">
      <div className="listNavbar">
        <div className="justifyButton">
          <OutsideClickHandler
            onOutsideClick={() => {
              setShow(false);
            }}
          >
            <FontAwesomeIcon
              icon={faAlignJustify}
              className="justifyIcon"
              onClick={() => {
                setShow(!isShow);
              }}
            />
          </OutsideClickHandler>
        </div>
        <div className="iconHome">
          <NavLink to="/">
            <FontAwesomeIcon icon={faHouseChimney} />
          </NavLink>
        </div>
        <Navbar
          options={navbar}
          style1="navbar"
          style={cln('navbar', { showNavbar: isShow })}
        />
      </div>
      <div className="groupUserIcon">
        <NavLink to="/login">
          <FontAwesomeIcon className="icon" icon={faUser} />
        </NavLink>
        <div className="inputSearch">
          <input
            type="text"
            name="search"
            className="textSpace"
            onChange={handleChangeSearchInput}
          />
          <FontAwesomeIcon
            className="icon iconSearch"
            icon={faMagnifyingGlass}
            onClick={handleSearchIcon}
          />
        </div>
        <div>
          <FontAwesomeIcon
            className="icon"
            icon={faShoppingBag}
            onClick={isOpenModal}
          />
          <div className="countItem">
            <div className="number">{cart.length}</div>
          </div>
        </div>
      </div>
      <Modal
        isOpen={openModal}
        onRequestClose={isCloseModal}
        contentLabel="Example Modal"
        /* style={modalStyles}*/
        className="Modal"
        overlayClassName="Overlay"
      >
        <ModalShoppingCart
          closeModal={isCloseModal}
          cart={cart}
          setCart={setCart}
          totalPrice={totalPrice}
          setTotalPrice={setTotalPrice}
          setStateProducts={setStateProducts}
          handleChange={handleChange}
        />
      </Modal>
    </div>
  );
};
export default Header;
