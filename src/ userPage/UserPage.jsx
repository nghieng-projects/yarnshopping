import React, { useEffect, useState } from 'react';

import { BrowserRouter, Routes, Route, useNavigate } from 'react-router-dom';
import Header from './header';
import Footer from './footer';
import HomePage from './homePage';
import DeTailProduct from './detailproduct';
import Login from './login';
import Register from './register';
import { calculatePrice } from '../utils/totalPrice';
import ItemPages from './itemPages';
import SearchPage from './searchPage';
import AddProduct from '../pages/addproduct';
import Product from '../pages/product/Product';
import InvoicePage from './invoicePage';
import ReviewPage from './reviewPage';
import './UserPage.scss';

const progress = [
  {
    name: 'Invoice',
    isShow: false,
  },
  {
    name: 'Payment',
    isShow: false,
  },
  {
    name: 'Review',
    isShow: false,
  },
  {
    name: 'Done',
    isShow: false,
  },
];
const UserPage = ({ notify }) => {
  const [currentStep, setCurrentStep] = useState(-1);

  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [totalPrice, setTotalPrice] = useState();
  const isOpenModal = () => {
    setOpenModal(!openModal);
  };
  const isCloseModal = () => {
    setOpenModal(false);
  };
  //
  const [cart, setCart] = useState([]);
  const setStateProducts = () => {
    const getCartLocal = JSON.parse(localStorage.getItem('cart'));
    //console.log({getCartLocal})
    setTotalPrice(calculatePrice(getCartLocal));
    setCart([...getCartLocal]);
  };
  const handleChange = (object, value) => {
    setCart((prevState) => {
      object.quantity = value;
      object.total = value * object.price;
      const index = cart.findIndex(
        (item) => item.idProduct === object.idProduct
      );
      prevState[index] = object;
      return [...prevState];
    });
  };
  const [search, setSearch] = useState('');
  const handleChangeSearchInput = ({ target }) => {
    const { value } = target;
    //console.log({value})
    setSearch(value);
  };

  const handleSearchIcon = () => {
    console.log(search);
    if (search !== '') {
      navigate(`/search?q=${search}`);
    } else navigate(`/`);
  };
  const [invoice, setInvoice] = useState(null);

  useEffect(() => {
    setStateProducts();
  }, []);
  useEffect(() => {
    localStorage.setItem('cart', JSON.stringify(cart));
    setTotalPrice(calculatePrice(cart));
  }, [cart]);

  return (
    <div className="userPage">
      <Header
        openModal={openModal}
        isOpenModal={isOpenModal}
        isCloseModal={isCloseModal}
        setStateProducts={setStateProducts}
        cart={cart}
        setCart={setCart}
        totalPrice={totalPrice}
        setTotalPrice={setTotalPrice}
        handleChange={handleChange}
        handleChangeSearchInput={handleChangeSearchInput}
        handleSearchIcon={handleSearchIcon}
      />
      <div className="Body">
        <Routes>
          <Route
              path="/review"
              element={
                <ReviewPage
                    steps={progress}
                    currentStep={currentStep}
                    setCurrentStep={setCurrentStep}
                    setCart={setCart}
                />
              }
          />
          <Route
              path="/invoice"
              element={
                <InvoicePage
                    cart={cart}
                    setCart={setCart}
                    handleChange={handleChange}
                    totalPrice={totalPrice}
                    setTotalPrice={setTotalPrice}
                    steps={progress}
                    currentStep={currentStep}
                    setCurrentStep={setCurrentStep}
                    invoice={invoice}
                    setInvoice={setInvoice}
                />
              }
          />
          <Route
              path="/add-product"
              element={<AddProduct notify={notify} />}
          />
          <Route path="/product" element={<Product />} />
          {/*useQuery no declare*/}
          <Route
              path={`/search`}
              element={<SearchPage loading={loading} setLoading={setLoading} />}
          />
          {/*useParams*/}
          <Route
              path={`/item/:category`}
              element={<ItemPages loading={loading} setLoading={setLoading} />}
          />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route
              path="/detail"
              element={<DeTailProduct cart={cart} setCart={setCart} />}
          />
          <Route path="/" element={<HomePage />} />
        </Routes>
      </div>
      <Footer />
    </div>
  );
};
export default UserPage;
