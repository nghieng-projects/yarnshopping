import React, { useEffect, useState } from 'react';
import './InvoicePage.scss';
import {
  getDistricts,
  getProvinces,
  getWards,
} from '../../Fetcher/address.fetcher';
import { calculatePrice } from '../../utils/totalPrice';
import ProgressIndicator from '../progressIndicator';
import pic from '../../assets/images/pic1.jpg';
import QuantityButton from '../../components/QuantityButton';
import CustomerInformation from './CustomerInformation';
import Modal from 'react-modal';
import PaymentPage from '../paymentPage';
import { useNavigate } from 'react-router-dom';
import { postUserPayment } from '../../Fetcher/payment.fetcher';

const options = ['', '', 'Price', 'Quantity', 'Total'];
const method = ['COD', 'Credit Card'];
const progress = [
  {
    name: 'Invoice',
    isShow: false,
  },
  {
    name: 'Payment',
    isShow: false,
  },
  {
    name: 'Review',
    isShow: false,
  },
  {
    name: 'Done',
    isShow: false,
  },
];

const InvoicePage = ({
  cart,
  setCart,
  totalPrice,
  setTotalPrice,
  handleChange,
  steps,
  currentStep,
  setCurrentStep,
  invoice,
  setInvoice,
}) => {
  const navigate = useNavigate();

  const [provinces, setProvinces] = useState([]);
  const [wards, setWards] = useState([]);
  const [districts, setDistricts] = useState([]);
  const [address, setAddress] = useState({
    province: '',
    ward: '',
    district: '',
  });
  const loadProvince = () => {
    getProvinces().then((dataCity) => {
      const array = [];
      var value;
      for (value in dataCity) {
        array.push(dataCity[value]);
      }
      setProvinces([...array]);
    });
  };
  const handlerProvincesSelect = (event) => {
    const code = event.target.value;
    // if check code = "" , value = 'Tinh/Thanh' => will value === "" , data not enough value, make progress indicator not change color
    if (code !== '') {
      const index = event.nativeEvent.target.selectedIndex;
      const getText = event.nativeEvent.target[index].text;

      getWards(code).then((ward) => {
        setWards([...ward]);
      });
      setAddress((prevState) => {
        return { ...prevState, province: getText };
      });
    } else {
      setAddress((prevState) => {
        return { ...prevState, province: '' };
      });
    }
  };
  const handlerWardsSelect = (event) => {
    const code = event.target.value;
    // if check code = "" , value = 'Tinh/Thanh' => will value === "" , data not enough value, make progress indicator not change color
    if (code !== '') {
      const index = event.nativeEvent.target.selectedIndex;
      const getText = event.nativeEvent.target[index].text;
      getDistricts(code).then((district) => {
        setDistricts([...district]);
      });
      setAddress((prevState) => {
        return { ...prevState, ward: getText };
      });
    } else {
      setAddress((prevState) => {
        return { ...prevState, ward: '' };
      });
    }
  };
  const handlerDistrictsSelect = (event) => {
    const code = event.target.value;
    // if check code = "" , value = 'Tinh/Thanh' => will value === "" , data not enough value, make progress indicator not change color
    if (code !== '') {
      const index = event.nativeEvent.target.selectedIndex;
      const getText = event.nativeEvent.target[index].text;

      setAddress((prevState) => {
        return { ...prevState, district: getText };
      });
    } else {
      setAddress((prevState) => {
        return { ...prevState, district: '' };
      });
    }
  };

  const shipFee = totalPrice >= 50 ? 0 : 10;
  const [userInformation, setUserInfromation] = useState({
    fullname: '',
    phone: '',
    address: '',
    email: '',
    methodPay: 'COD',
  });

  const handlerOnchangeInput = ({ target }) => {
    const { name, value } = target;
    setUserInfromation((prevState) => {
      return { ...prevState, [name]: value };
    });
  };
  const [methodPayment, setMethodPayment] = useState({});
  const handlerSelect = ({ target }) => {
    const { name, value } = target;
    setMethodPayment((prevState) => {
      return { ...prevState, methodPay: value };
    });
  };

  const [idpayment, setIdpayment] = useState(null);
  const submit = () => {
    const payLoadPrice = {
      subtotal: totalPrice,
      shipping: shipFee,
      finalPrice: totalPrice + shipFee,
    };

    const payLoadInformation = {
      ...userInformation,
      ...address,
      items: cart,
      priceProducts: payLoadPrice,
      ...methodPayment,
    };
    setInvoice(payLoadInformation);
    console.log({ payLoadInformation });
    if (
      userInformation.address &&
      userInformation.email &&
      userInformation.fullname &&
      userInformation.phone &&
      address.province &&
      address.ward &&
      address.district
    ) {
      postUserPayment(payLoadInformation).then((paymentData) => {
        console.log({ paymentData });
        if (paymentData.methodPay === 'Credit Card') {
          isOpenModalPayment();
          setIdpayment(paymentData.id);
          setCurrentStep(1);
        } else {
          navigate(`../review?id=${paymentData.id}`);
          setCurrentStep(2);
        }
      });
      localStorage.setItem('information', JSON.stringify(payLoadInformation));
    } else {
      alert('not enough information');
    }
  };

  const [openModalPayMent, setOpenModalPayment] = useState(false);
  const isOpenModalPayment = () => {
    setOpenModalPayment(!openModalPayMent);
  };
  const isCloseModalPayment = () => {
    setOpenModalPayment(false);
  };

  useEffect(() => {
    setTotalPrice(calculatePrice(cart));
    loadProvince();
    setCurrentStep(0);
  }, []);
  return (
    <div className="invoicePage">
      {/*<ProgressIndicator steps={steps} currentStep={currentStep}/>*/}
      <table cellPadding="0" cellSpacing="0">
        <thead>
          <tr>
            {options.map((title, index) => {
              return <th key={title}>{title}</th>;
            })}
          </tr>
        </thead>
        <tbody>
          {cart.map((item) => {
            return (
              <tr key={item.id}>
                <td>
                  <img src={pic} alt="" />
                </td>
                {/*width='120px' height='120'*/}
                <td>
                  <p>{item.product}</p>
                  <a href="#" className="removeBtn">
                    Remove
                  </a>
                </td>
                <td>{item.price}</td>
                <td>
                  <QuantityButton
                    value={item.quantity}
                    style={'quantityButton scaleItemPaymentPage'}
                    onChangeValue={(value) => {
                      handleChange(item, value);
                    }}
                  />
                </td>
                <td>{item.total}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className="bill">
        <div className="totalPrice">
          <p>Subtotal: </p>
          <h4>${totalPrice}.00</h4>
        </div>
        <div className="totalPrice">
          <p>Shipping & Handling: </p>
          <h4>${shipFee}.00</h4>
        </div>
        <div className="totalPrice">
          <p>Total Estimated: </p>
          <h4>${totalPrice + shipFee}.00</h4>
        </div>
      </div>
      <div className="groupInformation">
        <ProgressIndicator steps={progress} currentStep={currentStep} />
        <CustomerInformation
          handlerInput={handlerOnchangeInput}
          userInformation={userInformation}
          provinces={provinces}
          wards={wards}
          districts={districts}
          handlerProvinces={handlerProvincesSelect}
          handlerWards={handlerWardsSelect}
          handlerDistricts={handlerDistrictsSelect}
          method={method}
          handlerSelect={handlerSelect}
        />

        <div className="purchaseBtn">
          <button onClick={submit}>Next</button>
        </div>
      </div>
      <Modal
        isOpen={openModalPayMent}
        onRequestClose={isCloseModalPayment}
        contentLabel="Example Modal"
        /* style={modalStyles}*/
        className="PaymentModal"
        overlayClassName="Overlay"
      >
        <PaymentPage
          steps={steps}
          currentStep={currentStep}
          setCurrentStep={setCurrentStep}
          isCloseModal={isCloseModalPayment}
          invoice={invoice}
          setInvoice={setInvoice}
          idPayment={idpayment}
        />
      </Modal>
    </div>
  );
};
export default InvoicePage;
