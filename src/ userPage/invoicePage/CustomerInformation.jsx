import React, { useEffect } from 'react';
import './CustomerInformation.scss';
import Input from '../../components/Input';

const CustomerInformation = ({
  provinces,
  wards,
  districts,
  handlerProvinces,
  handlerDistricts,
  handlerWards,
  handlerInput,
  userInformation,
  method,
  handlerSelect,
}) => {
  return (
    <div className="informationUser">
      <div>
        <h3>Customer's Information</h3>
      </div>
      <div className="boxForm">
        <div className="inputInformation">
          <p>Fullname</p>
          <Input
            name="fullname"
            value={userInformation.fullname}
            placeholder="Fullname"
            handleChange={handlerInput}
          />
        </div>
        <div className="inputInformation">
          <p>Phone's number</p>
          <Input
            name="phone"
            placeholder={"Phone's number"}
            value={userInformation.phone}
            handleChange={handlerInput}
          />
        </div>
        <div className="inputInformation">
          <p>Delivery address</p>
          <Input
            name="address"
            placeholder="Address"
            value={userInformation.address}
            handleChange={handlerInput}
          />
        </div>
        <div className="inputInformation">
          <p>Email</p>
          <Input
            type={'email'}
            name="email"
            isRequired={true}
            placeholder="Email"
            value={userInformation.email}
            handleChange={handlerInput}
          />
        </div>
        <div className="selectInformation">
          <div>
            <p>Tinh/Tp:</p>
            <select name="" id="" onChange={handlerProvinces}>
              <option value={''}>Tinh/Thanh</option>
              {provinces.map((item) => {
                return (
                  <option key={item.name} value={item.code}>
                    {item.name}
                  </option>
                );
              })}
            </select>
            <select
              name=""
              id=""
              onChange={handlerWards}
              disabled={wards.length === 0}
            >
              <option value={''}>Quan/Huyen</option>
              {wards.map((item) => {
                return (
                  <option key={item.name} value={item.code}>
                    {item.name}
                  </option>
                );
              })}
            </select>
            <select
              name=""
              id=""
              onChange={handlerDistricts}
              disabled={districts.length === 0}
            >
              <option value={''}>Phuong/Xa</option>
              {districts.map((item) => {
                return (
                  <option key={item.name} value={item.code}>
                    {item.name}
                  </option>
                );
              })}
            </select>
          </div>
          <div style={{ marginTop: '1.5em' }}>
            <p>Payment Method:</p>
            <select name="methodPay" id="" onChange={handlerSelect}>
              {method.map((item) => {
                return (
                  <option key={item} value={item}>
                    {item}
                  </option>
                );
              })}
            </select>
          </div>
        </div>
      </div>
    </div>
  );
};
export default CustomerInformation;
