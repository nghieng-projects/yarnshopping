import React from 'react';
import './Footer.scss';
import AdminPage from '../../pages/mainpage';
import { BrowserRouter } from 'react-router-dom';
import { notifySuccess } from '../../utils/toastify.util';

const Footer = () => {
  const hostname = window.location.origin;

  return (
    <div className="footer">
      <p>Sweetie Yarns</p>
      <p>Street 16, Ward 8, District 2</p>
      <p>sweetiesSwettiesYarn@gmail.com</p>
      <a href={`${hostname}/product`} className={'linkAdmin'}>
        Admin link
      </a>
      {/*`${hostname}/admin/add-product`*/}
    </div>
  );
};
export default Footer;
