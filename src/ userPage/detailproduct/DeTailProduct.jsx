import React, {useEffect, useState} from 'react'
import './DetailProduct.scss'
import pic1 from '../../assets/images/pic1.jpg'
import QuantityButton from "../../components/QuantityButton";
import Button from "../../components/Button";
import {useLocation} from "react-router-dom";
import {getApiBaseId} from "../../Fetcher/yarnshopping.fetcher";
import {notifySuccess} from "../../utils/toastify.util";
import {ToastContainer} from "react-toastify";


const useQuery = () => {
    const {search} = useLocation();
    return React.useMemo(() => new URLSearchParams(search), [search]);
};

const LOCAL_CART_KEY = 'cart';

const DeTailProduct = ({cart, setCart}) => {
    const query = useQuery();
    const id = query.get('id');
    //setState for every product
    const [product, setProduct] = useState({
        idProduct: "",
        product: "",
        category: "",
        price: "",
        description:""
    })
    const [quantity, setQuantiTy] = useState(1)

    const addToCart = () => {
        let localCart = localStorage.getItem(LOCAL_CART_KEY);
        if (!localCart) {
            localCart = '[]';
        }
        const parsedLocalCart = JSON.parse(localCart);
        const cartProduct = {...product, quantity, total: quantity * product.price};
        const filterProduct = parsedLocalCart.findIndex((cart) => {
            return cart.idProduct === cartProduct.idProduct
        })
        if (filterProduct === -1) {
            parsedLocalCart.push(cartProduct);
            localStorage.setItem(LOCAL_CART_KEY, JSON.stringify(parsedLocalCart))
            console.log({parsedLocalCart});
            setCart([...parsedLocalCart])
            notifySuccess('successfully!')
        } else {
            parsedLocalCart[filterProduct] = cartProduct;
            localStorage.setItem(LOCAL_CART_KEY, JSON.stringify(parsedLocalCart))
            setCart([...parsedLocalCart])
            notifySuccess('successfully')
        }

    }

    useEffect(() => {
        getApiBaseId(id).then((data) => {
            //console.log({data})
            setProduct({
                idProduct: data.id,
                product: data.product,
                category: data.category,
                price: data.price,
                description: data.description
            })
        })
    }, [])


    return (
        <div className='DetailProduct'>
            <div className="imageSide">
                <div className="mainPicture">
                    <img src={pic1} alt=""/>
                </div>
                <div className="subPicture">
                    <img src={pic1} alt=""/>
                    <img src={pic1} alt=""/>
                    <img src={pic1} alt=""/>
                    <img src={pic1} alt=""/>
                    <img src={pic1} alt=""/>
                </div>
            </div>
            <div className="contentSide">
                <div className="titleProduct">
                    {/*<h5>{product.id}</h5>*/}
                    <p className="category">{product.category}</p>
                    <p className="nameProduct">{product.product}</p>
                    <p className="priceProduct">${product.price}.00</p>
                </div>
                <div className="quantity">
                    <QuantityButton value={quantity} style={'quantityButton'} onChangeValue={setQuantiTy}/>
                </div>
                <div className="addButton">
                    <Button text='Add to card' style="primary width100" onClick={addToCart}/>
                </div>

                <ToastContainer
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
            </div>
            <div className='introPart'>
                <div className="about">
                    <h3>About</h3>
                </div>
                <div className="introduceProduct">
                    <p className="information">{product.description}</p>

                </div>

            </div>
        </div>
    )
}
export default DeTailProduct