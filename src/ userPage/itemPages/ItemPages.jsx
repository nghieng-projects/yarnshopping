import React, { useEffect, useState } from 'react';
import styles from './ItemPages.scss';
import SampleProduct from '../../components/SampleProduct';
import { useParams } from 'react-router-dom';
import { filterApi } from '../../Fetcher/yarnshopping.fetcher';
import Spinner from '../../components/Spinner';

const ItemPages = ({ loading, setLoading }) => {
  let { category } = useParams();
  //console.log({category})
  const [item, setItem] = useState([]);
  //const [loading,setLoading] = useState(false)
  useEffect(() => {
    setLoading(true);
    filterApi(category).then((data) => {
      //console.log({data})
      setLoading(false);
      setItem([...data]);
    });
  }, [category]);

  return (
    <div className={'itemPages'}>
      {loading === false ? <SampleProduct products={item} /> : <Spinner />}
    </div>
  );
};
export default ItemPages;
