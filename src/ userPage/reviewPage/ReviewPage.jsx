import React, { useEffect, useState } from 'react';
import './ReviewPage.scss';
import { useLocation, useNavigate } from 'react-router-dom';
import { getPaymentBaseId } from '../../Fetcher/payment.fetcher';
import Modal from 'react-modal';
import SuccessOrder from './SuccessOrder';
import ReviewListProduct from './ReviewListProduct';
import Button from '../../components/Button';
import ProgressIndicator from '../progressIndicator';

const useQuery = () => {
  const { search } = useLocation();
  return React.useMemo(() => new URLSearchParams(search), [search]);
};
const ReviewPage = ({ steps, currentStep, setCurrentStep, setCart }) => {
  const navigate = useNavigate();
  const query = useQuery();
  const id = query.get('id');
  console.log(id);
  const [userPayment, setUserPayment] = useState();
  const [openModal, setOpenModal] = useState(false);
  const isOpenModal = () => {
    setOpenModal(!openModal);
  };
  const isCloseModal = () => {
    setOpenModal(false);
  };
  const [numberOrder, setNumberOrder] = useState('');
  const randomNumberOrder = () => {
    const type = [
      'a',
      'b',
      'B',
      'd',
      '1',
      '2',
      '3',
      '4',
      '5',
      '6',
      '7',
      '8',
      '9',
    ];
    const arr = [];
    if (arr.length < 10) {
      for (var i = 0; i < type.length; i++) {
        var num = type[Math.floor(Math.random() * type.length)];
        arr.push(num);
      }
    }
    const str = '000xx' + arr.join('');
    setNumberOrder(str);
  };

  const submitPayment = () => {
    isOpenModal();
  };

  useEffect(() => {
    randomNumberOrder();
    getPaymentBaseId(id).then((getData) => {
      console.log(getData);
      setUserPayment(getData);
      setCurrentStep(2);
    });
  }, []);

  return (
    <div className="reviewPage">
      <ProgressIndicator steps={steps} currentStep={currentStep} />
      {userPayment && (
        <div className="borderReviewPage">
          <h2 className="centerTitle">Review Order</h2>
          <p className="numberOrder">No.{numberOrder}</p>
          <div className="shippingInformation">
            <div className="address">
              <h4>Store's address</h4>
              <p className="groupName">Sweeties Yarn</p>
              <p>
                Light Town, 169bis, Lovely Street, Sunshine Ward, Sunset
                District
              </p>
            </div>
            <div className="address">
              <h4>Customer's address</h4>
              <p className="groupName">{userPayment.fullname}</p>
              <p>Phone number: {userPayment.phone}</p>
              <p>Address: {userPayment.address}</p>
              <p>Payment method: {userPayment.methodPay}</p>
              {userPayment.methodPay !== 'COD' ? (
                <div>
                  <p>Card number:</p>
                  <p>xxxxx{userPayment.cardNumber.slice(-4)}</p>
                </div>
              ) : (
                ''
              )}
            </div>
          </div>
          <div className="listProduct">
            <table>
              <thead>
                <tr>
                  <th>Product</th>
                  <th>Price</th>
                  <th>Quantity</th>
                </tr>
              </thead>
              <tbody>
                {userPayment.items.map((item) => {
                  return <ReviewListProduct item={item} />;
                })}
              </tbody>
            </table>
          </div>
          <div className="price">
            <div className="setRight">
              <div className="detail">
                <h4>Subtotal: </h4>
                <p>${userPayment.priceProducts.subtotal}.00</p>
              </div>
              <div className="detail">
                <h4>Shipping: </h4>
                <p>${userPayment.priceProducts.shipping}.00</p>
              </div>
              <div className="detail">
                <h4>FinalPrice: </h4>
                <h4>${userPayment.priceProducts.finalPrice}.00</h4>
              </div>
              {userPayment.methodPay === 'COD' ? (
                <p className="note">
                  ***You will receive items after 2-3 days.{' '}
                </p>
              ) : (
                <p className="note">
                  ***You will receive items after 2-3 days, from the day you
                  complete this payment.{' '}
                </p>
              )}
            </div>
          </div>
          <div className="methodPayment"></div>
          <div className="flexButton">
            <Button
              style={'default reviewPageButtonSize'}
              text={'Back'}
              onClick={() => {
                navigate('../invoice');
              }}
            />
            <Button
              style={'default reviewPageButtonSize'}
              text={'Pay now'}
              onClick={submitPayment}
            />
          </div>
        </div>
      )}

      <div>
        <Modal
          isOpen={openModal}
          onRequestClose={isCloseModal}
          className="successIconModal"
          overlayClassName="Overlay"
        >
          <SuccessOrder
            setCurrentStep={setCurrentStep}
            id={id}
            setCart={setCart}
          />
        </Modal>
      </div>
    </div>
  );
};
export default ReviewPage;
