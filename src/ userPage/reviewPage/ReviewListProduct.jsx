import React from "react";
import './ReviewListProduct.scss';

const ReviewListProduct = ({item}) => {
    return (
        <div className='reviewListProduct'>
            <tr>
                <td>{item.product}</td>
                <td>${item.price}</td>
                <td>{item.quantity}</td>
            </tr>
        </div>
    )
}
export default ReviewListProduct