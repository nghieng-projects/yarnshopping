import React, { useEffect } from 'react';
import './SuccessOrder.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClipboardCheck } from '@fortawesome/free-solid-svg-icons';
import Button from '../../components/Button';
import { useNavigate } from 'react-router-dom';
import {
  getPaymentBaseId,
  updateUserPayment,
} from '../../Fetcher/payment.fetcher';

const SuccessOrder = ({ setCurrentStep, id, setCart }) => {
  const navigate = useNavigate();
  const submit = () => {};

  useEffect(() => {
    setCurrentStep(4);
    getPaymentBaseId(id).then((dataPayment) => {
      const updateStatusPayment = {
        ...dataPayment,
        statusPayment: dataPayment.methodPay === 'COD' ? 'unpaid' : 'paid',
      };
      updateUserPayment(id, updateStatusPayment).then((lastData) => {
        console.log({ lastData });
      });
    });
    setCart([]);
    localStorage.removeItem('information');
    localStorage.removeItem('cart');
  }, []);
  return (
    <div className="successOrder">
      <div className="borderSuccessOrder">
        <div className="iconSuccess">
          <FontAwesomeIcon icon={faClipboardCheck} className="iCon" />
        </div>
        <div className="thankyouText">
          <h3>Your order is complete!</h3>
          <p>Thank you so much for your order.</p>
        </div>
        <div className="backButton">
          <Button
            text={'Home'}
            style={'reviewPageButton'}
            onClick={() => {
              navigate('../');
            }}
          />
        </div>
      </div>
    </div>
  );
};
export default SuccessOrder;
