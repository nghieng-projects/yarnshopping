import React, {useState} from "react";
import './Register.scss';
import Input from "../../components/Input";
import Button from "../../components/Button";
import GmailLogin from "../../components/GmailLogin";
import FacebookLogin from "../../components/FacebookLogin";
import {notifySuccess} from "../../utils/toastify.util";
import {useNavigate,NavLink} from "react-router-dom";

const Register = ({notify}) => {
    const navigate = useNavigate()
    const [userdata,setUserData] = useState({
        email:'',
        firstNumber:'+84',
        lastNumber:'',
        password:''
    })
    const handleOnChange = ({target}) =>{
        const {name,value} = target
        setUserData((prevState) =>{
            return {
                ...prevState,[name]:value
            }
        })
    }
    const postUserData = () => {
        if(userdata.email&&userdata.firstNumber&&userdata.lastNumber&&userdata.password){
            if(userdata.firstNumber[0] === "+"){
                fetch('http://localhost:3000/register',{
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(userdata)
                }).then((response)=>{
                    return response.json()
                }).then((data)=>{
                    console.log({data})
                    notifySuccess('successfull!')
                    setTimeout(()=>{navigate('/')},4000)
                    notify()

                })
            }else{
                const firstSymbol = '+'
                userdata.firstNumber = firstSymbol.concat(userdata.firstNumber)
                fetch('http://localhost:3000/register',{
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(userdata)
                }).then((response)=>{
                    return response.json()
                }).then((data)=>{
                    console.log({data})
                    notifySuccess('successfull!')
                    setTimeout(()=>{navigate('/')},4000)
                    notify()
                })
            }
        }else{
            console.log('not enough information')
        }
    }
    return(
        <div className="Register">
            <div className="box">
                <div className="content">
                    <h4>Sign up</h4>
                    <div className="inputPart">
                        <div className="Input">
                            <p>Email</p>
                            <Input name="email" value={userdata.email} type="email" placeholder="Type email" handleChange={handleOnChange}/>
                        </div>
                        <div className="Input">
                            <p>Phone</p>
                            <div className="phoneSpace">
                                <div className="firstPhoneNumber">
                                    <Input name="firstNumber" value={userdata.firstNumber } type="text" placeholder="+84" handleChange={handleOnChange}/>
                                </div>
                                <Input name="lastNumber" value={userdata.lastNumber} type="text" placeholder="Phone" handleChange={handleOnChange}/>
                            </div>
                        </div>
                        <div className="Input">
                            <p>Create Password</p>
                            <Input name="password" value={userdata.password} type="password" placeholder="Password" handleChange={handleOnChange}/>
                        </div>
                    </div>
                    <div className="privacityPolicy">
                        <p>By signing up, you confirm that you’ve read and accepted our User Notice and Privacy Policy.</p>
                    </div>
                    <div className="buttonPart">
                        <div>
                           <Button text={'Register'} style={'primary'} className="registerButton" onClick={postUserData} />
                        </div>
                        <div className="privacityPolicy">
                            <p>or register with</p>
                        </div>
                        <div className="gmailButton">
                            <div className="buttonItem">
                                <GmailLogin text={'Google'} className="registerButton"/>
                            </div>
                            <div className="buttonItem">
                                <FacebookLogin text={'Facebook'} className="registerButton"/>
                            </div>
                        </div>
                        <div className="textButton">
                            <p>Already have account? <NavLink to='/' className="linkColor" >Sign in</NavLink></p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
}
export default Register
