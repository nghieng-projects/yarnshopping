import React, { useEffect, useState } from 'react';
import './PaymentPage.scss';
import master from '../../assets/images/mastercard.png';
import visa from '../../assets/images/visacard.png';
import CustomerCard from './CustomerCard';
import ProgressIndicator from '../progressIndicator';
import { NavLink, useLocation, useNavigate } from 'react-router-dom';
import { updateUserPayment } from '../../Fetcher/payment.fetcher';

const cards = [
  {
    nameCard: 'Master Card',
    img: 'master',
  },
  {
    nameCard: 'Visa Card',
    img: 'visa',
  },
];

const PaymentPage = ({
  invoice,
  setInvoice,
  cart,
  setCart,
  steps,
  currentStep,
  setCurrentStep,
  isCloseModal,
  idPayment,
}) => {
  const navigate = useNavigate();
  const [userCard, setUserCard] = useState({
    cardType: '',
    cardNumber: '',
    cardHolder: '',
    cardExpired: '',
    cardCVC: '',
  });

  const handlerOnchangeInput = ({ target }) => {
    const { name, value } = target;
    setUserCard((prevState) => {
      return { ...prevState, [name]: value };
    });
    console.log({ value });
  };
  const [indexCard, setIndexCard] = useState(null);
  const cardOptions = cards.map((item) => {
    return item.nameCard;
  });
  const handleSelectCard = (event) => {
    const value = event.target.value;
    console.log({ value });
    const findIndexCard = cardOptions.findIndex((card) => {
      return card === value;
    });
    setIndexCard(findIndexCard);
    setUserCard((prevState) => {
      return { ...prevState, cardType: value };
    });
  };

  const submit = () => {
    if (
      userCard.cardType &&
      userCard.cardNumber &&
      userCard.cardHolder &&
      userCard.cardExpired &&
      userCard.cardCVC
    ) {
      const payLoadInformation = { ...userCard, ...invoice };
      console.log({ payLoadInformation });
      updateUserPayment(idPayment, payLoadInformation).then((paymentData) => {
        console.log({ paymentData });
        navigate(`../review?id=${idPayment}`);
      });
      localStorage.setItem('information', JSON.stringify(payLoadInformation));

      setInvoice(payLoadInformation);
      setCurrentStep(3);
      isCloseModal();
    } else {
      alert('do not have enough information');
    }
  };

  useEffect(() => {}, []);
  return (
    <div className="paymentPage">
      <ProgressIndicator steps={steps} currentStep={currentStep} />
      <CustomerCard
        indexCard={indexCard}
        handleSelectCard={handleSelectCard}
        cardOptions={cardOptions}
        userCard={userCard}
        handlerOnchangeInput={handlerOnchangeInput}
      />
      <div className="groupBtn flexButton">
        <div className="purchaseBtn">
          <button onClick={isCloseModal}>Back</button>
        </div>
        <div className="purchaseBtn">
          <button onClick={submit}>Next</button>
        </div>
      </div>
    </div>
  );
};
export default PaymentPage;
