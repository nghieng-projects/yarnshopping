import React, {useState} from "react";
import './CustomerCard.scss';
import Select from "../../components/Select";
import master from "../../assets/images/mastercard.png";
import visa from "../../assets/images/visacard.png";
import Input from "../../components/Input";


const CustomerCard = ({handleSelectCard, indexCard, cardOptions, userCard, handlerOnchangeInput}) => {
    return (
        <div className='selectCard'>
            <p>Please select your card</p>
            <Select options={cardOptions} placeholder={'Select card'} handleSelect={handleSelectCard}/>
            <div className='card'>
                {
                    indexCard === 0 ? (<img src={master} alt=""/>) : (<img src={visa} alt=""/>)
                }
            </div>
            <div className='cardInformation'>
                <div className='inputInformation'>
                    <p>Card number</p>
                    <Input name={'cardNumber'} value={userCard.cardNumber} handleChange={handlerOnchangeInput}/>
                </div>
                <div className='inputInformation'>
                    <p>Card Holder</p>
                    <Input name={'cardHolder'} value={userCard.cardHolder} handleChange={handlerOnchangeInput}/>
                </div>
                <div className='inputInformation'>
                    <p>Expires</p>
                    <Input name={'cardExpired'} value={userCard.cardExpired} handleChange={handlerOnchangeInput}/>
                </div>
                <div className='inputInformation'>
                    <p>CVC</p>
                    <Input name={'cardCVC'} value={userCard.cardCVC} handleChange={handlerOnchangeInput}/>
                </div>
            </div>
        </div>
    )
}
export default CustomerCard
