import React, {useEffect, useState} from 'react';
import './SearchPage.scss';
import {useLocation} from "react-router-dom";
import {filterApi} from "../../Fetcher/yarnshopping.fetcher";
import SampleProduct from "../../components/SampleProduct";
import Spinner from "../../components/Spinner";


function useQuery() {
    const {search} = useLocation();
    return React.useMemo(() => new URLSearchParams(search), [search]);
}

const SearchPage = ({loading,setLoading}) => {
    let query = useQuery();
    const [item, setItem] = useState([])
    const string = query.get("q")
    //const [loading,setLoading] = useState(false)
    useEffect(() => {
        console.log({string})
        setLoading(true)
        filterApi(string).then((data) => {
            console.log({data})
            setItem([...data])
            setLoading(false)
        })

    }, [string])

    return (
        <div className='searchPage'>
            {
                loading === false ? (<SampleProduct products={item}/>) : (<Spinner/>)
            }
        </div>
    )
}
export default SearchPage