import React, {useState, useEffect} from 'react';
import './modalShoppingCart.scss';
import pic1 from '../../assets/images/pic1.jpg'
import QuantityButton from "../../components/QuantityButton";
import Button from "../../components/Button";
import TextFreeship from "../../components/TextFreeship";
import {notifySuccess} from "../../utils/toastify.util";
import { useNavigate} from "react-router-dom";

const cartKey ='cart'
const ModalShoppingCart = ({closeModal, cart,setCart,totalPrice,setStateProducts,handleChange}) => {
    const navigate = useNavigate()

    const removeItem = (object) => {
        const parseArr = JSON.parse(localStorage.getItem(cartKey))
        const confirm = window.confirm('Do you want to delete this item?')
        if(confirm){
            const filterItemLeft = parseArr.filter((item)=>{return item.idProduct !== object.idProduct})
            if(cartKey){
                localStorage.setItem(cartKey,JSON.stringify(filterItemLeft))
                setCart(filterItemLeft)
                notifySuccess('successfully')
            }
        }
    }
    // calculation the price when load page
    const [disable,setDisable] = useState(false)
    const [styleButton,setStyleButton] = useState('default width100 modalBtn')
    const handleClickCheckOut = () => {
        if(cart.length > 0){
            setDisable(true)
            setStyleButton('default width100 modalBtnDisable')
            navigate('./invoice')
            closeModal()
        }else {
            setDisable(false)
        }
    }
    useEffect(()=>{
        setStateProducts()
    },[])


    return (
        <div className="modalCart">
            <div className='titleModal'>
                <h3>Cart</h3>
                <button onClick={closeModal}>X</button>
            </div>
            <div className='bodyCard'>
                <div className='listItems'>
                    {
                        cart.map((item,index) => {
                            return (
                                <div className='list' key={index}>
                                    <div className="imageSide">
                                        <img src={pic1} alt=""/>
                                    </div>
                                    <div className='information'>
                                        <p className='nameProduct'>{item.product}</p>
                                        <p className='priceProduct'>${item.price}.00</p>
                                        <div className='quantityNprice'>
                                            <QuantityButton value={item.quantity} style={'quantityButton'} onChangeValue={(value)=>{handleChange(item, value)}}/>
                                            <p className='price'>${item.price * item.quantity}.00</p>
                                        </div>
                                        <a href="#" onClick={()=>{removeItem(item)}}>Remove</a>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
                <div className='finalPrice'>
                    <h4>Subtotal</h4>
                    <p className='priceTotal'>${totalPrice}.00</p>
                </div>
                <div className='part'>
                    <TextFreeship bill={totalPrice}/>
                </div>
                <div className='part'>
                    <Button text={'Review & Checkout'} disable={disable} onClick={handleClickCheckOut} style={styleButton} />
                </div>
            </div>


        </div>
    )
}
export default ModalShoppingCart