import React, {useState} from 'react'
import {useNavigate,NavLink} from "react-router-dom";
import './Login.scss'
import Input from "../../components/Input";
import Button from "../../components/Button";
import GmailLogin from "../../components/GmailLogin";
import FacebookLogin from "../../components/FacebookLogin";

const loginSuccess = {
    status:"success",
    token:'abcdefghd'
}

const Login = () => {
    const navigate = useNavigate()

    const [user,setUser] = useState({
        username: "",
        password: ""
    })
    const handleOnChange = ({target}) => {
        const {name,value} = target
        setUser((prevState) => {
            return {...prevState,[name]: value}
        })
    }
    const onLogin = async () =>{
        const data = await Promise.resolve(
            loginSuccess
        )
        console.log(data)
        navigate('/add-product')
        return data
    }


    return (
        <div className="Login">
            <div className="box">
                <div className="content">
                   <div className="part">
                       <h4>Sign in</h4>
                   </div>
                    <div className="part">
                        <div className="Input">
                            <Input name="email" type="email" value={user.username} placeholder="Username or email" handleChange={handleOnChange}/>
                        </div>
                        <div className="Input">
                            <Input name="password" type="password" value={user.password} placeholder="Password" handleChange={handleOnChange}/>
                        </div>

                    </div>
                    <div className="part">
                        <div className="selectCheckbox">
                            <input className="checkbox" type="checkbox"/><span>Remember</span>
                        </div>
                        <div>
                            <a href="" className="linkColor">Forgot password?</a>
                        </div>
                    </div>
                    <div className="part">
                        <div className="item">
                            <Button text="Login" style="primary" className="loginButton"  onClick={onLogin}/>
                        </div>
                        <div className="item textButton">
                            <p>Don't have account? <NavLink to='/register' className="linkColor" >Sign up</NavLink></p>
                        </div>
                        <div className="item">
                            <GmailLogin text={'Login using Gmail'} className="loginButton"/>
                        </div>
                        <div className="item">
                            <FacebookLogin text={'Login using facebook'} className="loginButton"/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
}
export default Login